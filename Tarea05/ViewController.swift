//
//  ViewController.swift
//  Tarea05
//
//  Created by Javier Yllescas on 11/19/19.
//  Copyright © 2019 Javier Yllescas. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapa: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapa.delegate = self
        
        //puntero
        let nombrePuntero:String = "BEDU"
        let subtituloPuntero:String = "Clase iOS"
        let coordenadas:(Lat:Double, Long:Double) = (19.3746, -99.1626)
        let location = CLLocationCoordinate2D(latitude: coordenadas.Lat, longitude: coordenadas.Long)
        //puntero2
        let nombrePuntero2:String = "Monumento a la Independencia"
        let coordenadas2:(Lat:Double, Long:Double) = (19.4269, -99.1674)
        let location2 = CLLocationCoordinate2D(latitude: coordenadas2.Lat, longitude: coordenadas2.Long)
        //puntero3
        let nombrePuntero3:String = "Mi Casa"
        let subtituloPuntero3:String = "Donde yo vivo"
        let coordenadas3:(Lat:Double, Long:Double) = (19.3108, -99.1566)
        let location3 = CLLocationCoordinate2D(latitude: coordenadas3.Lat, longitude: coordenadas3.Long)
        //Region
        let focus = CLLocationCoordinate2D(latitude: 19.3480, longitude: -99.1600)
        let puntero = MKCoordinateSpan(latitudeDelta: 0.08, longitudeDelta: 0.08)
        let region = MKCoordinateRegion(center: focus, span: puntero)
        mapa.setRegion(region, animated: true)
        
        //Locacion
        let pin = MKPointAnnotation()
        pin.coordinate = location
        pin.title = nombrePuntero
        pin.subtitle = subtituloPuntero
        
        let pin2 = MKPointAnnotation()
        pin2.coordinate = location2
        pin2.title = nombrePuntero2
        
        let pin3 = MKPointAnnotation()
        pin3.coordinate = location3
        pin3.title = nombrePuntero3
        pin3.subtitle = subtituloPuntero3
        
        mapa.addAnnotation(pin)
        mapa.addAnnotation(pin3)
        
        //Agrego la ruta
        let mark01 = MKPlacemark(coordinate: location, addressDictionary: nil)
        let mark02 = MKPlacemark(coordinate: location3, addressDictionary: nil)
        let inicio = MKMapItem(placemark: mark01)
        let final = MKMapItem(placemark: mark02)
        
        crearRuta(inicio: inicio, final: final)
        
        /*
        //tuplas
        let coordenadas: (lat:Double, long:Double, nombre:String) = (19.426980, -99.167696, "Bellas Artes")
        
        print("Latitud: \(coordenadas.lat), Longuitud: \(coordenadas.long), Nombre: \(coordenadas.nombre)")
        
        let yo: (nombre:String, edad:Int, estatura:Float) = ("Javier Yllescas", 24, 1.69)
        print("Nombre: \(yo.0)")
        print("Edad: \(yo.edad)")
        print("Estatura: \(yo.estatura)")
        
        //Declarar tupla a constantes
        let coordenada3d = (a:2, b:3, c:4)
        let (a, b, c) = coordenada3d
        print(a)
        print(b)
        print(c)
        
        let (d, e, _) = (coordenada3d)
        print(d)
        print(e)
         */
    }
    
    func crearRuta(inicio:MKMapItem, final:MKMapItem){
        let direccion = MKDirections.Request()
        direccion.source = inicio
        direccion.destination = final
        direccion.transportType = .automobile
        
        let auxDireccion = MKDirections(request: direccion)
        auxDireccion.calculate{ (response, error) -> Void in
            guard let response = response else {return}
            let ruta = response.routes[0]
            self.mapa.addOverlay((ruta.polyline), level:MKOverlayLevel.aboveRoads)
            let rect = ruta.polyline.boundingMapRect
            self.mapa.setRegion(MKCoordinateRegion(rect), animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4.0
        return renderer
    }

}

